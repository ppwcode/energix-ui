import { appShouldBeAtRoute } from '../common/routing/path'

export const aboutButtonSelector: string = '[id=about]'

describe('About', () => {
    it('should display the about page when navigating to the about section', () => {
        cy.visit('/')
        appShouldBeAtRoute('#/home')
        cy.get(aboutButtonSelector).click()
        appShouldBeAtRoute('#/about')
    })
})
