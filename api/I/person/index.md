<!--
  ~ Copyright 2021 – 2021 PeopleWare
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an “AS IS” BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->

Natural persons that potentially have ownership of shares. The person resource gathers the existence of the resource,
and personal information about the person.

An _ownership_ is a relationship between a natural person and a _stock_, during a certain interval.

Resources are natural persons for which an _ownership_ is or was believed to exist

When, at some time, such a relationship was believed to exist, and with later knowledge, we believe it never did, the
person information is still stored and retained in the system, for reasons of audit.

## Business key

The business keys for a person are the person's `INSS`s. At least one `INSS` is mandatory at every moment during the
life of a person resource.

An INSS uniquely refers to 1 person over time. The Belgian government never re-uses a INSS for a different person.

At every moment relevant to the system, there is one `INSS` that is intended by the Belgian government to represent a
relevant person, but that `INSS` might change over time. We only learn later that the `INSS` which the Belgian
government intends to be used to represent a relevant person has changed. Not all organizations learn about the change
at the same time.

Persons are found by search not only on the most recent `INSS`, but on any `INSS` that was associated with them during
the existence of the resource.

Since the INSS is considered sensitive, in this case we cannot use a business key in the URL for the resource. We use
the `persistenceId` as surrogate key instead.
