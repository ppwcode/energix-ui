<!--
  ~ Copyright 2021 – 2021 PeopleWare
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an “AS IS” BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->

Stocks are resources of which natural persons can own shares. The stock resource gathers the existence of the resource,
and information about the stock.

## Business key

The business keys for a stock is its _ticker symbol_, which is guaranteed to be unique. The ticker symbol of a stock can
never change.

<div style="background-color: gray;"><strong>NOTE:</strong> In real live, it can, but not in this demo project.</div>
