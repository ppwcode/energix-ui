/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
export interface Env {
    /**
     * Padded build identification of the immutable build of this SPA.
     */
    immutableBuild: string

    /**
     * Padded build identification of the bookmarkable build of this SPA.
     *
     * In `test` and `prod`, this is taken from `energix-ui-bookmarkable/config.js`, where it is filled out
     * on deploy by CI. In all other situations, it is `00000` which is mentioned in `energix-ui/src/app/config.js`.
     */
    bookmarkableBuild: string
}

interface Window {
    env: Env
    location: Location
}
