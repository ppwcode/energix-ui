import { Spectator, createComponentFactory } from '@ngneat/spectator'

import { AboutComponent } from './about.component'
import { ConfigService } from '../shared/services/config.service'
import { MatCardModule } from '@angular/material/card'

describe('AboutComponent', () => {
    let spectator: Spectator<AboutComponent>
    const createComponent = createComponentFactory({
        component: AboutComponent,
        imports: [MatCardModule],
        mocks: [ConfigService]
    })

    it('should create', () => {
        spectator = createComponent()

        expect(spectator.component).toBeTruthy()
    })
})
