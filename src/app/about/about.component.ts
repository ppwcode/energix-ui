import { ChangeDetectionStrategy, Component } from '@angular/core'
import { ConfigService } from '../shared/services/config.service'

@Component({
    selector: 'energix-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AboutComponent {
    constructor(public configService: ConfigService) {}
}
