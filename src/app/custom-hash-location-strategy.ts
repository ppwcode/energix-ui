// eslint-disable-next-line
/* istanbul ignore file */

import { HashLocationStrategy } from '@angular/common'
import { Injectable } from '@angular/core'

@Injectable()
export class CustomLocationStrategy extends HashLocationStrategy {
    public override prepareExternalUrl(internal: string): string {
        return `${this.getBaseHref()}#${internal}`
    }
}
