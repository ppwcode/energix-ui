/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Injectable } from '@angular/core'
import { MsalBroadcastService, MsalService } from '@azure/msal-angular'
import { BehaviorSubject, Observable } from 'rxjs'

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    loggedIn$!: Observable<boolean>

    private _isLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)

    constructor(public readonly authService: MsalService, private readonly msalBroadcastService: MsalBroadcastService) {
        this.loggedIn$ = this._isLoggedIn$.asObservable()

        this._isLoggedIn$.next(this.checkLoggedIn())

        this.msalBroadcastService.msalSubject$.subscribe(() => {
            this._isLoggedIn$.next(this.checkLoggedIn())
        })
    }

    login(): void {
        this.authService.loginRedirect()
    }

    logOut(): void {
        this.authService.logoutRedirect()
    }

    private checkLoggedIn(): boolean {
        return this.authService.instance.getAllAccounts().length > 0
    }
}
