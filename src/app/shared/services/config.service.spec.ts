import { createServiceFactory, SpectatorService } from '@ngneat/spectator'
import { ConfigService } from './config.service'
import { Env } from '../../../../schemata/global/window'
import { WINDOW_TOKEN, windowProvider } from './window'

const env: Env = {
    immutableBuild: 'IMMUTABLE',
    bookmarkableBuild: 'BOOKMARKABLE'
}

const windowMock = {
    env
}

describe('ConfigService', () => {
    let spectator: SpectatorService<ConfigService>
    const createService = createServiceFactory({
        service: ConfigService,
        imports: [],
        providers: [{ provide: WINDOW_TOKEN, useValue: windowMock }]
    })

    beforeEach(() => (spectator = createService()))

    it('should initialize correctly', () => {
        expect(spectator.service.bookmarkableBuild).toEqual(env.bookmarkableBuild)
        expect(spectator.service.immutableBuild).toEqual(env.immutableBuild)
    })

    it('should provide the window correctly', () => {
        const provided = windowProvider()
        expect(provided).toEqual(window)
    })
})
