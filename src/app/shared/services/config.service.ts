/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import { Inject, Injectable } from '@angular/core'

import { Window } from '../../../../schemata/global/window'
import { WINDOW_TOKEN } from './window'

/**
 * The `ConfigService` is a fixed point for the entire app. The app cannot proceed if the properties of this object are
 * not known.
 *
 * The values of this object are in principle defined in the `energix-ui-bookmarkable`.
 *
 */
@Injectable({
    providedIn: 'root'
})
export class ConfigService {
    readonly immutableBuild: string
    readonly bookmarkableBuild: string

    // eslint-disable-next-line complexity
    constructor(@Inject(WINDOW_TOKEN) private readonly window: Window) {
        this.immutableBuild = this.window.env.immutableBuild
        this.bookmarkableBuild = this.window.env.bookmarkableBuild

        this.initialize()
    }

    initialize(): void {
        // istanbul ignore next
        // disable tslint rule, we want to log some config info
        // eslint-disable-next-line no-console
        console.log(
            `%c

 _______                        _
(_______)                      (_)
 _____   ____   ____  ____ ____ _ _   _
|  ___) |  _ \\ / _  )/ ___) _  | ( \\ / )
| |_____| | | ( (/ /| |  ( ( | | |) X (
|_______)_| |_|\\____)_|   \\_|| |_(_/ \\_)
                         (_____|



Immutable Build: ${this.immutableBuild}
Bookmarkable Build: ${this.bookmarkableBuild}`,
            'color: #bada55'
        )
    }
}
