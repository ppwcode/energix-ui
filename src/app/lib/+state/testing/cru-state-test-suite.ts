/* eslint-disable max-lines-per-function */
/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Type } from '@angular/core'
import { createServiceFactory, SpectatorService, SpectatorServiceFactory } from '@ngneat/spectator'
import { NgxsModule, Store } from '@ngxs/store'
import { AbstractCruState, CruStateModel, PresentationMode } from '@ppwcode/ng-vernacular-cru'
import { NgxsDispatchPluginModule } from '@ngxs-labs/dispatch-decorator'
import { NgxsSelectSnapshotModule } from '@ngxs-labs/select-snapshot'

/**
 * This test suite can be used to verify the implementation of a state.
 * @param suiteName The name of the test suite. Most likely the same as the name of the state.
 * @param state The state type.
 * @param testingResource The resource instance to use during testing.
 */
export const cruStateTestSuite = <
    TResource extends Record<string, unknown>,
    TState extends AbstractCruState<TResource, CruStateModel<TResource>>
>(
    suiteName: string,
    state: Type<TState>,
    testingResource: TResource
): void => {
    describe(suiteName, () => {
        let spectator: SpectatorService<TState>
        let resource: TResource
        let store: Store
        const createService: SpectatorServiceFactory<TState> = createServiceFactory({
            service: state,
            imports: [
                NgxsModule.forRoot([state], { developmentMode: true }),
                NgxsDispatchPluginModule.forRoot(),
                NgxsSelectSnapshotModule.forRoot()
            ]
        })

        beforeEach(() => {
            spectator = createService()
            resource = testingResource
            store = spectator.inject(Store)
        })

        it('should select a resource', () => {
            expect(store.selectSnapshot(spectator.service.token).selectedResource).toBeNull()

            spectator.service.selectResource(resource)
            expect(store.selectSnapshot(spectator.service.token).selectedResource).toBe(resource)

            spectator.service.selectResource(null)
            expect(store.selectSnapshot(spectator.service.token).selectedResource).toBeNull()
        })

        Object.values(PresentationMode)
            .filter((pm: PresentationMode) => isNaN(parseInt(pm)))
            .forEach((mode: PresentationMode) => {
                it(`should change the presentation mode to ${mode}`, () => {
                    expect(store.selectSnapshot(spectator.service.token).presentationMode).toEqual(
                        PresentationMode.UNSET
                    )

                    spectator.service.setPresentationMode(mode)

                    expect(store.selectSnapshot(spectator.service.token).presentationMode).toEqual(mode)
                })
            })
        ;[true, false].forEach((isBusy: boolean) => {
            it(`should update the busy state to ${isBusy}`, () => {
                expect(store.selectSnapshot(spectator.service.token).isBusy).toEqual(false)

                spectator.service.setBusy(isBusy)

                expect(store.selectSnapshot(spectator.service.token).isBusy).toEqual(isBusy)
            })
        })
    })
}
