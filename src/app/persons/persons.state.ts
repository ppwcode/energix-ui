import { Injectable } from '@angular/core'
import { State } from '@ngxs/store'
import { AbstractCruState, CruStateModel, getDefaultCruState } from '@ppwcode/ng-vernacular-cru'

import { Person } from './interfaces/person'

export declare type PersonsStateModel = CruStateModel<Person>

const STATE_IDENTIFIER = 'persons'

@State<PersonsStateModel>({
    name: STATE_IDENTIFIER,
    defaults: getDefaultCruState<Person>()
})
@Injectable()
export class PersonsState extends AbstractCruState<Person, PersonsStateModel> {
    protected stateIdentifier: string = STATE_IDENTIFIER
}
