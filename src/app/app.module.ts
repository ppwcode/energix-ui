import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { TranslateLoader, TranslateModule } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin'
import { NgxsModule } from '@ngxs/store'
import { CruResourcesManagerService, PpwcodeVernacularCruModule } from '@ppwcode/ng-vernacular-cru'

import { AppComponent } from './app.component'
import { AppRoutingModule } from './app-routing.module'
import { HomeComponent } from './home/home.component'
import { MiniPresentationsModule } from './mini-presentations/mini-presentations.module'
import { PersonsFacade } from './persons/persons.facade'
import { PersonsState } from './persons/persons.state'
import { StocksFacade } from './stocks/stocks.facade'
import { StocksState } from './stocks/stocks.state'
import { MatIconModule } from '@angular/material/icon'
import { MatListModule } from '@angular/material/list'
import { AboutComponent } from './about/about.component'
import { MatCardModule } from '@angular/material/card'
import { WINDOW_TOKEN, windowProvider } from './shared/services/window'
import { NgxsDispatchPluginModule } from '@ngxs-labs/dispatch-decorator'
import { NgxsSelectSnapshotModule } from '@ngxs-labs/select-snapshot'
import { LoginComponent } from './login/login.component'
import { MatFormFieldModule } from '@angular/material/form-field'
import { sharedModules } from './shared/shared-modules.constant'
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button'
import {
    MsalBroadcastService,
    MsalGuard,
    MsalInterceptor,
    MsalModule,
    MsalRedirectComponent,
    MsalService
} from '@azure/msal-angular'
import { BrowserCacheLocation, InteractionType, PublicClientApplication } from '@azure/msal-browser'
import { MatProgressBarModule } from '@angular/material/progress-bar'

export const createTranslateLoader = (http: HttpClient): TranslateHttpLoader =>
    new TranslateHttpLoader(http, './assets/i18n/', '.json')

@NgModule({
    declarations: [AppComponent, AboutComponent, HomeComponent, LoginComponent],
    imports: [
        ...sharedModules,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        PpwcodeVernacularCruModule,
        NgxsModule.forRoot([PersonsState, StocksState]),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        NgxsDispatchPluginModule.forRoot(),
        NgxsSelectSnapshotModule.forRoot(),
        HttpClientModule,
        MiniPresentationsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        MatIconModule,
        MatListModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MsalModule.forRoot(
            new PublicClientApplication({
                // MSAL Configuration
                auth: {
                    clientId: '16a52f9a-9fe3-4eb1-b923-77c8d1fe1411',
                    authority: 'https://login.microsoftonline.com/6bf21748-a694-4bd1-b1c4-6563b75bde06',
                    redirectUri: 'http://localhost:4200',
                    postLogoutRedirectUri: 'http://localhost:4200'
                },
                cache: {
                    cacheLocation: BrowserCacheLocation.LocalStorage
                },
                system: {
                    loggerOptions: {
                        loggerCallback: () => {},
                        piiLoggingEnabled: false
                    }
                }
            }),
            {
                interactionType: InteractionType.Redirect // MSAL Guard Configuration
            },
            {
                interactionType: InteractionType.Redirect,
                protectedResourceMap: new Map([['https://graph.microsoft.com/v1.0/me', ['user.read']]])
            }
        ),
        MatProgressBarModule
    ],
    providers: [
        {
            provide: WINDOW_TOKEN,
            useFactory: windowProvider
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: MsalInterceptor,
            multi: true
        },
        MsalService,
        MsalGuard,
        MsalBroadcastService
    ],
    bootstrap: [AppComponent, MsalRedirectComponent]
})
export class AppModule {
    constructor(cruResourceManager: CruResourcesManagerService) {
        cruResourceManager.registerResource('/I/person', PersonsFacade)
        cruResourceManager.registerResource('/I/stock', StocksFacade)
    }
}
