import { ChangeDetectionStrategy, Component } from '@angular/core'
import { AuthService } from '../shared/services/auth.service'

@Component({
    selector: 'energix-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent {
    constructor(public auth: AuthService) {}
}
