import { createComponentFactory, Spectator } from '@ngneat/spectator'

import { HomeComponent } from './home.component'
import { MsalModule } from '@azure/msal-angular'
import { BrowserCacheLocation, InteractionType, PublicClientApplication } from '@azure/msal-browser'
import { LoginComponent } from '../login/login.component'
import { MatCardModule } from '@angular/material/card'
import { TranslateModule } from '@ngx-translate/core'

describe('HomeComponent', () => {
    let spectator: Spectator<HomeComponent>
    const createComponent = createComponentFactory({
        component: HomeComponent,
        declarations: [LoginComponent],
        imports: [
            MatCardModule,
            TranslateModule.forRoot(),
            MsalModule.forRoot(
                new PublicClientApplication({
                    // MSAL Configuration
                    auth: {
                        clientId: '16a52f9a-9fe3-4eb1-b923-77c8d1fe1411',
                        authority: 'https://login.microsoftonline.com/6bf21748-a694-4bd1-b1c4-6563b75bde06',
                        redirectUri: 'http://localhost:4200',
                        postLogoutRedirectUri: 'http://localhost:4200'
                    },
                    cache: {
                        cacheLocation: BrowserCacheLocation.LocalStorage
                    },
                    system: {
                        loggerOptions: {
                            loggerCallback: () => {},
                            piiLoggingEnabled: false
                        }
                    }
                }),
                {
                    interactionType: InteractionType.Redirect // MSAL Guard Configuration
                },
                {
                    interactionType: InteractionType.Redirect,
                    protectedResourceMap: new Map([['https://graph.microsoft.com/v1.0/me', ['user.read']]])
                }
            )
        ]
    })

    it('should create', () => {
        spectator = createComponent()

        expect(spectator.component).toBeTruthy()
    })
})
