import { Injectable } from '@angular/core'
import { State } from '@ngxs/store'
import { AbstractCruState, CruStateModel, getDefaultCruState } from '@ppwcode/ng-vernacular-cru'

import { Stock } from '../../../schemata/stock/Stock'

export declare type StocksStateModel = CruStateModel<Stock>

const STATE_IDENTIFIER = 'stocks'

@State<StocksStateModel>({
    name: STATE_IDENTIFIER,
    defaults: getDefaultCruState<Stock>()
})
@Injectable()
export class StocksState extends AbstractCruState<Stock, StocksStateModel> {
    protected stateIdentifier: string = STATE_IDENTIFIER
}
