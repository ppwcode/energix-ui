import { APP_BASE_HREF, LocationStrategy } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { CustomLocationStrategy } from './custom-hash-location-strategy'
import { HomeComponent } from './home/home.component'
import { AboutComponent } from './about/about.component'
import { MsalGuard } from '@azure/msal-angular'

const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        canActivate: []
    },
    {
        path: 'about',
        component: AboutComponent,
        canActivate: [MsalGuard]
    },
    {
        path: 'persons',
        // eslint-disable-next-line @typescript-eslint/typedef
        loadChildren: () => import('./persons/persons.module').then((m) => m.PersonsModule),
        canActivate: [MsalGuard]
    },
    {
        path: 'stocks',
        // eslint-disable-next-line @typescript-eslint/typedef
        loadChildren: () => import('./stocks/stocks.module').then((m) => m.StocksModule),
        canActivate: [MsalGuard]
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: 'home'
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            relativeLinkResolution: 'corrected'
        })
    ],
    exports: [RouterModule],
    providers: [
        { provide: APP_BASE_HREF, useValue: location.pathname },
        { provide: LocationStrategy, useClass: CustomLocationStrategy }
    ]
})
export class AppRoutingModule {}
