#!/usr/bin/env node

const { BlobServiceClient } = require('@azure/storage-blob')
const moment = require('moment-timezone')

const account = 'energixuiartifacts'
const sas = process.env.DESTINATION_SAS_TOKEN
const containerName = 'immutable'
const title = 'Energix Main UI - Immutable'
const timezone = 'Europe/Brussels'

const blobServiceClient = new BlobServiceClient(`https://${account}.blob.core.windows.net${sas}`)

async function main() {
    const containerClient = blobServiceClient.getContainerClient(containerName)

    console.log('Getting immutable builds …')
    let blobs = containerClient.listBlobsByHierarchy('/')
    let builds = []
    for await (const blob of blobs) {
        if (blob.kind === 'prefix') {
            builds.push({ prefix: blob.name, name: blob.name.replace('/', ''), lastModified: undefined })
        }
    }
    for (const build of builds) {
        let buildItems = containerClient.listBlobsByHierarchy('/', { prefix: build.prefix })
        for await (const blob of buildItems) {
            if (blob.kind !== 'prefix') {
                build.lastModified = blob.properties.lastModified
                break
            }
        }
    }
    builds.sort((a, b) => (a.name < b.name ? 1 : -1))

    const body = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>${title}</title>
    </head>
    <body style="font-family: Roboto, 'Helvetica Neue', sans-serif">
        <h1>${title}</h1>
        <ul>
    ${builds
        .map(
            (i) =>
                `        <li><a href="${i.prefix}index.html">${
                    i.name
                }</a> <span style="font-size: 80%; color: gray; margin-left: 1em">${moment(i.lastModified)
                    .tz('Europe/Brussels')
                    .format('ddd, DD MMM YYYY @ HH:mm:ss')} (${timezone})</span></li>`
        )
        .join('\n')}
        </ul>
    </body>
    </html>`

    const content = body
    const blobName = 'index.html'
    const blockBlobClient = containerClient.getBlockBlobClient(blobName)
    const uploadBlobResponse = await blockBlobClient.upload(content, content.length, {
        blobHTTPHeaders: { blobContentType: 'text/html' }
    })
    console.log(`Upload ${blobName} successfully`, uploadBlobResponse.requestId)
}

main()
